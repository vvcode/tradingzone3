﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SQLite;
using System.Configuration;


namespace LinqExamples1 {
    public partial class TestWebForm1 : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e) {
            //List<Broker> brokers = Brokers;
        }

        private SQLiteConnection _conn;
        public SQLiteConnection Conn {
            get {
                if (_conn == null) {
                    string connectionString = ConfigurationManager.ConnectionStrings["onedconn"].ConnectionString;
                    _conn = new SQLiteConnection(connectionString);
                    return _conn;
                }
                else {
                    return _conn;
                }
            }
        }



        /*public IEnumerable<Broker> Brokers {
            get {
                List<Broker> brokers = new List<Broker>();

                SQLiteCommand cmd = new SQLiteCommand(Conn);
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "select * from tblBroker";
                if (Conn.State == ConnectionState.Closed) {
                    Conn.Open();
                }
                SQLiteDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read()) {
                    Broker broker = new Broker();
                    broker.BrokerId = Convert.ToInt32(rdr["broker_id"]);
                    broker.BrokerName = Convert.ToString(rdr["broker_name"]);
                    broker.BrokerType = Convert.ToString(rdr["broker_type"]);
                    broker.Brokerage = Convert.ToDouble(rdr["brokerage"]);
                    broker.FirmId = Convert.ToInt32(rdr["firm_id"]);
                    brokers.Add(broker);
                }

                return brokers;

            }
        }

        public int GetMaxBrokerId() {
            SQLiteCommand cmd = new SQLiteCommand(Conn);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select MAX(broker_id) from tblBroker";
            if (Conn.State == ConnectionState.Closed) {
                Conn.Open();
            }
            var rdr = cmd.ExecuteScalar();
            if (rdr != null) {
                return Convert.ToInt32(rdr) + 1;
            }
            else {
                return 1;
            }
        }

        public bool AddBroker(Broker broker) {
            SQLiteCommand cmd = new SQLiteCommand(Conn);
            var brokerid = GetMaxBrokerId();
            var brokername = broker.BrokerName;
            var brokertype = broker.BrokerType;
            var brokerage = broker.Brokerage;
            var firmid = broker.FirmId;

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "INSERT INTO tblBroker (broker_id, broker_name,broker_type,brokerage, firm_id)" +
                "VALUES (" + brokerid + ", '" + brokername + "', '" + brokertype + "', " + brokerage + ", " + firmid + ")";
            if (Conn.State == ConnectionState.Closed) {
                Conn.Open();
            }

            var rdr = cmd.ExecuteNonQuery();
            return true;
        }

        public bool UpdateBroker(Broker broker) {
            SQLiteCommand cmd = new SQLiteCommand(Conn);
            var brokerid = broker.BrokerId;
            var brokername = broker.BrokerName;
            var brokertype = broker.BrokerType;
            var brokerage = broker.Brokerage;
            var firmid = broker.FirmId;

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "UPDATE tblBroker SET broker_name = '" + brokername + "',broker_type = '" + brokertype + "', brokerage = " + brokerage + ", firm_id = " + firmid +
                " WHERE  broker_id = " + brokerid;
            if (Conn.State == ConnectionState.Closed) {
                Conn.Open();
            }

            var rdr = cmd.ExecuteNonQuery();
            return true;
        }

        public bool DeleteBroker(Broker broker) {
            SQLiteCommand cmd = new SQLiteCommand(Conn);
            var brokerid = broker.BrokerId;

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "DELETE FROM tblBroker WHERE  broker_id = " + brokerid;
            if (Conn.State == ConnectionState.Closed) {
                Conn.Open();
            }

            var rdr = cmd.ExecuteNonQuery();
            return true;

        } */
    }
}