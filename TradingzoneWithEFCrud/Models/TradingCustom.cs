﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TradingzoneWithEFCrud.Models {
    public class TradingCustom {

    }

    [MetadataType(typeof(BrokerMetaData))]
    public partial class Broker
    {
        
    }

    public class BrokerMetaData
    {
        [DisplayName("Broker ID")]
        public int broker_id { get; set; }

        [DisplayName("Broker Name")]
        public string broker_name { get; set; }

        [DisplayName("Broker Type")]
        public string broker_type { get; set; }

        [DisplayName("Brokerage")]
        public Nullable<double> brokerage { get; set; }

        [DisplayName("Firm ID")]
        public Nullable<int> firm_id { get; set; }
    }
}