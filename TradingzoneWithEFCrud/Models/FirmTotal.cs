﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TradingzoneWithEFCrud.Models {
    public class FirmTotal {
        public string FirmName { get; set; }
        public int BrokerCount { get; set; }
    }
}