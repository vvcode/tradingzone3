﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLayer;

namespace TradingZoneWithoutEF.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            BrokerBusinessLayer bl = new BrokerBusinessLayer();
            List<BusinessLayer.Broker> br = bl.Brokers.ToList();
            return View(br);
        }

        [ActionName("Create")]
        [HttpGet]
        public ActionResult Create_Get() {
            return View();
        }

        [ActionName("Create")]
        [HttpPost]
        public ActionResult Create_Post() {

            Broker broker = new Broker();
            
            
            TryUpdateModel(broker);

            if (ModelState.IsValid) {
                BrokerBusinessLayer bl = new BrokerBusinessLayer();
                bl.AddBroker(broker);

                return RedirectToAction("Index");
            }
            return View();
        }

        [HttpGet]
        public ActionResult Edit(int id) {
            BrokerBusinessLayer bl = new BrokerBusinessLayer();
            Broker broker = bl.Brokers.Single(b => b.BrokerId == id);
            return View(broker);
        }

        [HttpPost]
        [ActionName("Edit")]
        public ActionResult Edit_Post(int id)
        {
            BrokerBusinessLayer bl = new BrokerBusinessLayer();
            Broker broker = bl.Brokers.Single(b => b.BrokerId == id);
            UpdateModel<IBroker>(broker);

            if (ModelState.IsValid)
            {
                bl.UpdateBroker(broker);
                return RedirectToAction("Index");
            }
            return View(broker);
        }

        [HttpPost]
        public ActionResult Delete(int id) {
            BrokerBusinessLayer bl = new BrokerBusinessLayer();
            Broker broker = bl.Brokers.Single(b => b.BrokerId == id);
            bl.DeleteBroker(broker);
            return RedirectToAction("Index");
        }

        // One way of creating object
        /*[HttpPost]
        public ActionResult Create(FormCollection formCollection) {
            Broker broker = new Broker();
            broker.BrokerName = formCollection["BrokerName"];
            broker.BrokerType = formCollection["BrokerType"];
            broker.Brokerage = Convert.ToDouble(formCollection["Brokerage"]);
            broker.FirmId = Convert.ToInt32(formCollection["FirmId"]);

            BrokerBusinessLayer bl = new BrokerBusinessLayer();
            bl.AddBroker(broker);
            return RedirectToAction("Index");
        } */
    }
}
