﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace BusinessLayer {

    public interface IBroker
    {
        int BrokerId { get; set; }
        //string BrokerName { get; set; }
        string BrokerType { get; set; }
        double Brokerage { get; set; }
        int FirmId { get; set; }
        
    }

    public class Broker : IBroker {
        
        public int BrokerId { get; set; }

        [Required]
        public string BrokerName { get; set; }

        [Required]
        public string BrokerType { get; set; }

        [Required]
        public double Brokerage { get; set; }

        [Required]
        public int FirmId { get; set; }
    }
}
