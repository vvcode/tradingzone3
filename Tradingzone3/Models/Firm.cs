﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Tradingzone3.Models {

    [Table("tblFirm")]
    public class Firm {

        [Column("firm_id")]
        public int FirmId { get; set; }

        [Column("firm_name")]
        public string FirmName { get; set; }

        public List<Broker> Brokers { get; set; }
    }
}