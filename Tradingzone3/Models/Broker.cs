﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Tradingzone3.Models {

    [Table("tblBroker")]
    public class Broker {
        [Column("broker_id")]
        public int BrokerId { get; set; }

        [Column("broker_name")]
        public string BrokerName { get; set; }

        [Column("broker_type")]
        public string BrokerType { get; set; }

        [Column("brokerage")]
        public double Brokerage { get; set; }

        [Column("firm_id")]
        public int FirmId { get; set; }

    }
}