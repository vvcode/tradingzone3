﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Tradingzone3.Models {
    public class BrokerContext : DbContext {

        public DbSet<Broker> Brokers { get; set; }
        public DbSet<Firm> Firms { get; set; }
    }
}