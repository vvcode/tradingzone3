﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tradingzone3.Models;

namespace Tradingzone3.Controllers
{
    public class BrokerController : Controller
    {
        public ActionResult Index(int id) {
            BrokerContext brokerContext = new BrokerContext();
            List<Broker> brokers = brokerContext.Brokers.Where(brk => brk.FirmId == id).ToList();
            return View(brokers);
        }

        public ActionResult Details(int id)
        {
            //Broker broker = new Broker()
            //{
            //    BrokerId = 1,
            //    BrokerName = "John",
            //    BrokerType = "FIX",
            //    Brokerage = 3.5
            //};
            BrokerContext brokerContext = new BrokerContext();
            Broker broker = brokerContext.Brokers.Single(bkr => bkr.BrokerId == id);

            return View(broker);
        }

    }
}
