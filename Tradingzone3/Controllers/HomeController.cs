﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tradingzone3.Models;

namespace Tradingzone3.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            BrokerContext brokerContext = new BrokerContext();
            return View(brokerContext.Brokers.ToList());
        }

    }
}
